from decimal import Decimal


class Item():
    def __init__(self, name: str, price: Decimal, link: str):
        self.name = name
        self.price = price
        self.link = link