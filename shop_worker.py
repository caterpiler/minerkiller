import logging
import os
import re
import toml
import pathlib
from decimal import Decimal
from selenium import webdriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.remote.remote_connection import LOGGER as selenium_logger
from urllib3.connectionpool import log as ulrlib_logger
from typing import List
import chromedriver_autoinstaller

from utils import load_all_config_params
from entities import Item
from authenticator import Authenticator
from logger import logger


class Parser(object):
    def __init__(self):
        if os.environ.get('debug_mode', 'false') != 'true':
            selenium_logger.setLevel(logging.INFO)
            ulrlib_logger.setLevel(logging.INFO)
        with open(pathlib.Path(__file__).parent / 'ali_links.toml') as f:
            self.selectors = toml.load(f)
        chromedriver_autoinstaller.install()
        options = webdriver.ChromeOptions()
        options.add_experimental_option('excludeSwitches', ['enable-logging'])
        """
        We are setting default browser preferences explicitly
        to reduce possible privacy data leaking for antibot systems!
            1: Enabled
            2: Disabled
        """
        options.add_experimental_option("prefs", {
            "profile.default_content_setting_values.media_stream_mic": 2,
            "profile.default_content_setting_values.media_stream_camera": 2,
            "profile.default_content_setting_values.geolocation": 2,
            "profile.default_content_setting_values.notifications": 2
        })
        self.driver = webdriver.Chrome(options=options)
        delay_sec = 3
        self.driver.implicitly_wait(delay_sec)
        self.authenticator = Authenticator(self.driver, self.selectors)

    def _send_text(self, textbox_selector: str, text: str) -> None:
        element = self.driver.find_element_by_css_selector(textbox_selector)
        element.clear()
        element.send_keys(text)

    def _push_button(self, button_selector: str) -> None:
        button = self.driver.find_element_by_css_selector(button_selector)
        button.click()

    def _get_text(self, text_selector: str) -> str:
        text_element = self.driver.find_element_by_css_selector(text_selector)
        text = text_element.text
        return text

    def _next_page(self) -> None:
        self._push_button(self.selectors['NEXT_PAGE_BUTTON_ELEMENT'])

    def _get_elements_number(self) -> int:
        total_pages_text_element = self.driver.find_element_by_css_selector(self.selectors['PRODUCTS_NUMBER_ELEMENT'])
        text = total_pages_text_element.text
        # text example - '18 results'
        parse_digits = r'\d+'
        digits_list = re.findall(parse_digits, text)

        if len(digits_list) > 0:
            total_elements_number = int(digits_list[0])
        else:
            total_elements_number = 0

        return total_elements_number

    @staticmethod
    def _parse_price(text: str) -> str:
        text = text.lower()
        price = '0'
        if 'руб' in text:
            parse_rubles = r'[\d+,]'
            price = re.findall(parse_rubles, text)
            price = ''.join(price)
            price = price.replace(',', '.')
        if '$' in text:
            parse_dollars = r'[\d+.]'
            price = re.findall(parse_dollars, text)
            price = ''.join(price)
        return price

    @staticmethod
    def _parse_id(link: str) -> str:
        # link example - 'https://www.aliexpress.ru/item/1005003091886219.html'
        id_start_symbol = '/'
        id_end_symbol = '.'
        item_id = link[link.rfind(id_start_symbol) + 1:link.rfind(id_end_symbol)]
        return item_id

    def _get_elements_data(self, elements: List[WebElement]) -> List[Item]:
        items_list = []

        for element in elements:
            element_detail = element.find_element_by_css_selector(self.selectors['PRODUCT_DETAIL'])
            element_info = element_detail.find_element_by_css_selector('a')
            name = element_info.get_attribute('title')
            link = element_info.get_attribute('href')
            link = 'https://www.' + link[link.find('/') + 2:]
            price_text = element_detail.find_element_by_css_selector('b').get_attribute('innerHTML')
            price = self._parse_price(str(price_text))
            price = Decimal(price)
            items_list.append(Item(name, price, link))

        return items_list

    def _parse_elements(self, elements_selector: str) -> List[Item]:
        elements = self.driver.find_elements_by_css_selector(elements_selector)
        elements_on_page = len(elements)

        items_list = []
        if elements_on_page > 0:
            items_list = self._get_elements_data(elements)
            total_elements_number = self._get_elements_number()
            if total_elements_number > elements_on_page:
                if total_elements_number % elements_on_page == 0:
                    pages_number = total_elements_number // elements_on_page
                else:
                    pages_number = total_elements_number // elements_on_page + 1
                for page in range(1, pages_number):
                    self._next_page()
                    elements = self.driver.find_elements_by_css_selector(elements_selector)
                    temp_items = self._get_elements_data(elements)
                    items_list.extend(temp_items)

        return items_list

    def get_items(self, shop_link: str, product_name: str) -> List[Item]:
        logger.debug(f'Running Parser.get_items for product {product_name}, shop_link {shop_link}')
        self.driver.get(shop_link)
        # Write search request
        self._send_text(self.selectors['PRODUCT_SEARCH_ELEMENT'], product_name)
        self._push_button(self.selectors['PRODUCT_SEARCH_BUTTON_ELEMENT'])
        # Check if authorization appear
        authorization_element = self.driver.find_elements_by_css_selector(self.selectors['LOGIN_TEXTBOX'])
        if len(authorization_element) > 0:
            self.authenticator.authenticate(os.environ['LOGIN'], os.environ['PASSWORD'])
            return self.get_items(shop_link, product_name)
        # Get products information
        items_list = self._parse_elements(self.selectors['PRODUCTS_LIST_ELEMENT'])
        logger.debug('Finished Parser.get_items')
        return items_list

    def add_item_to_basket(self, link: str) -> bool:
        logger.debug(f'Running Parser.add_item_to_basket, link {link}')
        self.driver.get(link)
        item_id = self._parse_id(link)
        basket_items_id = []
        self._push_button(self.selectors['ADD_TO_BASKET_BUTTON_ELEMENT'])
        # After adding to basket wait until wrapper appears
        basket_wrapper = self.driver.find_elements_by_css_selector(self.selectors['BASCET_WRAPPER_ELEMENT'])
        if len(basket_wrapper) > 0:
            basket_items = self.check_basket()
            basket_items_links = [item.link for item in basket_items]
            basket_items_id = [self._parse_id(bi_link) for bi_link in basket_items_links]
        if item_id in basket_items_id:
            logger.debug('Finished Parser.add_item_to_basket, item added')
            return True
        else:
            logger.debug('Finished Parser.add_item_to_basket, adding failed')
            return False

    def _parse_basket_element(self, element: WebElement) -> Item:

        element_link = element.find_element_by_css_selector(self.selectors['PRODUCT_NAME'])
        link_text = element_link.get_attribute('href')
        link = link_text[:link_text.find('?')]
        name = element_link.get_attribute('innerHTML')
        cost_text = element.find_element_by_css_selector(self.selectors['PRODUCT_PRICE']).get_attribute('innerHTML')
        price = self._parse_price(str(cost_text))
        price = Decimal(price)

        return Item(name, price, link)

    def check_basket(self) -> List[Item]:
        logger.debug('Running Parser.check_basket')
        basket_links = self.driver.find_elements_by_css_selector(self.selectors['TO_BASKET_BUTTON_ELEMENT'])
        if len(basket_links) > 1:
            basket_links[1].click()
        else:
            basket_links[0].click()
        elements = self.driver.find_elements_by_css_selector(self.selectors['BASKET_ALL_PRODUCTS_ELEMENT'])

        items_list = []
        if len(elements) > 0:
            for element in elements:
                items_list.append(self._parse_basket_element(element))
        logger.debug('Finished Parser.check_basket')
        return items_list


if __name__ == '__main__':

    shop_link = 'https://citilink.aliexpress.ru/'
    item_name = 'rtx 3080'
    test_item_link = 'https://aliexpress.ru/item/1005003091886219.html'

    with open(pathlib.Path(__file__).parent / 'config' / 'local' / 'config.toml') as conf:
        conf = toml.load(conf)
        load_all_config_params(conf)

    agent = Parser()
    items = agent.get_items(shop_link, item_name)
    item_in_basket = agent.add_item_to_basket(items[0].link)
    # item_in_basket = agent.add_item_to_basket(test_item_link)
    if item_in_basket:
        print('Item in basket')
    items_in_basket = agent.check_basket()
    #
    for item in items_in_basket:
        print(f'{item.name}\n{item.price}\n{item.link}')

    agent.driver.quit()
