from pytest_mock import MockFixture
import pytest

from scalper import Scalper
from entities import Item
from shop_worker import Parser


@pytest.fixture
def parser(mocker: MockFixture):
    def __init__(self):
        self.driver = mocker.MagicMock()
        self.authenticator = mocker.MagicMock()
        self.selectors = mocker.MagicMock()

    mocker.patch.object(
        Parser,
        '__init__',
        __init__,
        spec=True
    )

    return Parser()


def test_scalp_item_when_item_to_buy_exists_scalp_it(mocker: MockFixture, parser: Parser):
    # Arrange
    item_name = "RTX 3090"
    max_price = 100
    suitable_item = Item(item_name, 100, "link_dummy")
    unsuitable_item = Item(item_name, 1000, "link_dummy")
    shop_items = [suitable_item, unsuitable_item]
    sclaper = Scalper(parser, "shop_link_dummy")
    mocker.patch("shop_worker.Parser.get_items", return_value=shop_items)
    mocker.patch("shop_worker.Parser.add_item_to_basket", return_value=True)

    # Act
    scalped_item = sclaper.scalp_item(item_name, max_price)

    # Assert
    assert scalped_item == suitable_item


def test_scalp_item_when_item_to_buy_not_exists_return_none(mocker: MockFixture, parser: Parser):
    # Arrange
    item_name = "RTX 3090"
    max_price = 100
    unsuitable_item = Item(item_name, 1000, "link_dummy")
    shop_items = [unsuitable_item]
    sclaper = Scalper(parser, "shop_link_dummy")
    mocker.patch("shop_worker.Parser.get_items", return_value=shop_items)
    mocker.patch("shop_worker.Parser.add_item_to_basket", return_value=True)

    # Act
    scalped_item = sclaper.scalp_item(item_name, max_price)

    # Assert
    assert scalped_item is None


def test_scalp_item_when_items_to_buy_empty_return_none(mocker: MockFixture, parser: Parser):
    # Arrange
    item_name = "RTX 3090"
    max_price = 100
    shop_items = []
    sclaper = Scalper(parser, "shop_link_dummy")
    mocker.patch("shop_worker.Parser.get_items", return_value=shop_items)
    mocker.patch("shop_worker.Parser.add_item_to_basket", return_value=True)

    # Act
    scalped_item = sclaper.scalp_item(item_name, max_price)

    # Assert
    assert scalped_item is None


def test_scalp_item_when_several_item_to_buy_exists_scalp_cheapest(mocker: MockFixture, parser: Parser):
    # Arrange
    item_name = "RTX 3090"
    max_price = 500
    first_unsuitable_item = Item(item_name, 200, "link_dummy")
    suitable_item = Item(item_name, 100, "link_dummy")
    second_unsuitable_item = Item(item_name, 300, "link_dummy")
    shop_items = [first_unsuitable_item, suitable_item, second_unsuitable_item]
    sclaper = Scalper(parser, "shop_link_dummy")
    mocker.patch("shop_worker.Parser.get_items", return_value=shop_items)
    mocker.patch("shop_worker.Parser.add_item_to_basket", return_value=True)

    # Act
    scalped_item = sclaper.scalp_item(item_name, max_price)

    # Assert
    assert scalped_item == suitable_item


def test_scalp_item_when_putting_to_basket_fails_return_none(mocker: MockFixture, parser: Parser):
    # Arrange
    item_name = "RTX 3090"
    max_price = 500
    suitable_item = Item(item_name, 100, "link_dummy")
    shop_items = [suitable_item]
    sclaper = Scalper(parser, "shop_link_dummy")
    mocker.patch("shop_worker.Parser.get_items", return_value=shop_items)
    mocker.patch("shop_worker.Parser.add_item_to_basket", return_value=False)

    # Act
    scalped_item = sclaper.scalp_item(item_name, max_price)

    # Assert
    assert scalped_item is None
