from pytest_mock import MockFixture
import pytest

from authenticator import Authenticator
from exceptions import AuthenticationError


stub_config = {
    'SLIDER_FIELD': 'DUMMY_SLIDER_FIELD',
    'SLIDER_BUTTON': 'DUMMY_SLIDER_BUTTON',
    'AUTHORIZATION_BUTTON': 'DUMMY_AUTHORIZATION_BUTTON',
    'GET_NEW_CAPTCHA': 'DUMMY_GET_NEW_CAPTCHA',
    'LOGIN_TEXTBOX': 'DUMMY_LOGIN_TEXTBOX',
    'PASSWORD_TEXTBOX': 'DUMMY_PASSWORD_TEXTBOX',
    'AUTHENTICATION_ERROR': 'DUMMY_AUTHENTICATION_ERROR'
}

username = 'user'
password = 'password'


@pytest.fixture
def authenticator(mocker: MockFixture):
    def __init__(self):
        self.driver = mocker.MagicMock()
        self.slider_unlocker = mocker.MagicMock()
        self.captcha_solver = mocker.MagicMock()
        self.selectors = mocker.MagicMock()
        self.selectors.__getitem__.side_effect = stub_config.__getitem__

    mocker.patch.object(
        Authenticator,
        '__init__',
        __init__,
        spec=True
    )

    return Authenticator()


def test_authenticate_when_successful_no_exceptions_is_raised(
    authenticator: Authenticator,
):
    try:
        authenticator.authenticate(username, password)
    except Exception as ex:
        assert False, f"'authenticate' raised an exception {ex}"


def test_authenticate_when_slider_is_found_calls_slider_unlocker(
    mocker: MockFixture,
    authenticator: Authenticator,
):
    def slider_is_found(*args):
        if args[0] == stub_config['SLIDER_FIELD']:
            return ['slider_element_stub']
        elif args[0] == stub_config['GET_NEW_CAPTCHA']:
            return []
        elif args[0] == stub_config['AUTHENTICATION_ERROR']:
            return []
        return None

    authenticator.driver.find_elements_by_css_selector = mocker.MagicMock(side_effect=slider_is_found)

    authenticator.authenticate(username, password)

    authenticator.slider_unlocker.unlock.assert_called_once()


def test_authenticate_when_captcha_is_found_rises_not_implemented(
    mocker: MockFixture,
    authenticator: Authenticator,
):
    def slider_is_found(*args):
        if args[0] == stub_config['SLIDER_FIELD']:
            return ['slider_element_stub']
        elif args[0] == stub_config['GET_NEW_CAPTCHA']:
            return ['captcha_element_stub']
        elif args[0] == stub_config['AUTHENTICATION_ERROR']:
            return []
        return None

    authenticator.driver.find_elements_by_css_selector = mocker.MagicMock(side_effect=slider_is_found)

    authenticator.authenticate(username, password)

    authenticator.captcha_solver.solve_captcha.assert_called_once()


def test_authenticate_when_error_is_found_rise_not_authentication_error(
    mocker: MockFixture,
    authenticator: Authenticator,
):
    def slider_is_found(*args):
        if args[0] == stub_config['SLIDER_FIELD']:
            return []
        elif args[0] == stub_config['GET_NEW_CAPTCHA']:
            return []
        elif args[0] == stub_config['AUTHENTICATION_ERROR']:
            return ['error_element_stub']
        return None

    authenticator.driver.find_elements_by_css_selector = mocker.MagicMock(side_effect=slider_is_found)

    with pytest.raises(AuthenticationError):
        authenticator.authenticate(username, password)