from pytest_mock import MockFixture

from notifier import Notifier


def test_send_message(mocker: MockFixture):
    # GIVEN
    n = Notifier()
    message = "test"
    mock_requests_get = mocker.patch(
        'notifier.requests.get'
    )

    # WHEN
    n.send_message(message)

    # THEN
    mock_requests_get.assert_called_once()
    assert mock_requests_get.call_args.args[1]['text'] == message
