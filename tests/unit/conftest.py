import pytest
import pathlib
import toml

from utils import load_all_config_params


@pytest.fixture(autouse=True)
def load_env_variables() -> None:
    with open(pathlib.Path(__file__).parent.parent.parent / 'config' / 'local' / 'config.toml') as conf:
        conf = toml.load(conf)
        load_all_config_params(conf)
