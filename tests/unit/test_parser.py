from pytest_mock import MockFixture
import pytest
from selenium.webdriver.remote.webelement import WebElement
from decimal import Decimal

from shop_worker import Parser
from entities import Item


class FakeText:
    def __init__(self, text: str) -> None:
        self.text = text


@pytest.fixture
def parser(mocker: MockFixture):
    def __init__(self):
        self.driver = mocker.MagicMock()
        self.authenticator = mocker.MagicMock()
        self.selectors = mocker.MagicMock()

    mocker.patch.object(
        Parser,
        '__init__',
        __init__,
        spec=True
    )

    return Parser()


def test_get_elements_number_when_number_not_exist(mocker: MockFixture, parser: Parser) -> None:
    # Arrange
    example_1 = FakeText('elements')
    parser.driver.find_element_by_css_selector = mocker.MagicMock(return_value=example_1)

    # Act
    total_elements_number = parser._get_elements_number()

    # Assert
    assert total_elements_number == 0


def test_get_elements_number_when_number_exist(mocker: MockFixture, parser: Parser) -> None:
    # Arrange
    example_2 = FakeText('159 elements')
    parser.driver.find_element_by_css_selector = mocker.MagicMock(return_value=example_2)

    # Act
    total_elements_number = parser._get_elements_number()

    # Assert
    assert total_elements_number == 159


def test_parse_price_rubles(parser: Parser) -> None:
    # Arrange
    price_text = '9 011,43 руб.'

    # Act
    price = parser._parse_price(price_text)

    # Assert
    assert price == '9011.43'


def test_parse_price_usd(parser: Parser) -> None:
    # Arrange
    price_text = 'US $31.92'

    # Act
    price = parser._parse_price(price_text)

    # Assert
    assert price == '31.92'


def test_parse_price_dummy(parser: Parser) -> None:
    # Arrange
    price_text = 'price_dummy'

    # Act
    price = parser._parse_price(price_text)

    # Assert
    assert price == '0'


def test_parse_id_normal(parser: Parser) -> None:
    # Arrange
    link = 'https://www.aliexpress.ru/item/1005003091886219.html'

    # Act
    item_id = parser._parse_id(link)

    # Assert
    assert item_id == '1005003091886219'


def test_get_elements_data_with_one_element(mocker: MockFixture, parser: Parser) -> None:
    # Arrange
    item = Item('title_dummy',
                Decimal('9011.43'),
                'https://www.aliexpress'
                )
    get_attributes_output = ['title_dummy',
                             'http://aliexpress',
                             '9 011,43 руб.']
    mocker.patch("shop_worker.WebElement.get_attribute",
                 side_effect=get_attributes_output)
    mocker.patch("shop_worker.WebElement.find_element_by_css_selector",
                 return_value=WebElement)
    elements = [WebElement]

    # Act
    item_list = parser._get_elements_data(elements)
    item_test = item_list[0]

    # Assert
    assert item_test.name == item.name
    assert item_test.link == item.link
    assert item_test.price == item.price


def test_get_elements_data_with_two_elements(mocker: MockFixture, parser: Parser) -> None:
    # Arrange
    item_1 = Item('title_dummy_1',
                  Decimal('9011.43'),
                  'https://www.aliexpress'
                  )
    item_2 = Item('title_dummy_2',
                  Decimal('1503.65'),
                  'https://www.aliexpress'
                  )
    suitable_items = [item_1, item_2]

    get_attributes_output_1 = ['title_dummy_1',
                               'http://aliexpress',
                               '9 011,43 руб.']
    get_attributes_output_2 = ['title_dummy_2',
                               'http://aliexpress',
                               '1 503,65 руб.']
    get_attributes_output = [*get_attributes_output_1, *get_attributes_output_2]
    mocker.patch("shop_worker.WebElement.get_attribute",
                 side_effect=get_attributes_output)
    mocker.patch("shop_worker.WebElement.find_element_by_css_selector",
                 return_value=WebElement)
    elements = [WebElement, WebElement]

    # Act
    item_list = parser._get_elements_data(elements)

    # Assert
    for i in range(len(item_list)):
        assert item_list[i].name == suitable_items[i].name
        assert item_list[i].link == suitable_items[i].link
        assert item_list[i].price == suitable_items[i].price


def test_get_elements_data_without_elements(mocker: MockFixture, parser: Parser) -> None:
    # Arrange
    get_attributes_output = ['title_dummy',
                             'http://aliexpress',
                             '9 011,43 руб.']
    mocker.patch("shop_worker.WebElement.get_attribute",
                 side_effect=get_attributes_output)
    mocker.patch("shop_worker.WebElement.find_element_by_css_selector",
                 return_value=WebElement)
    elements = []

    # Act
    item_list = parser._get_elements_data(elements)

    # Assert
    assert item_list == []


def test_parse_elements_when_elements_number_on_page_equal_to_total_elements_number(mocker: MockFixture, parser: Parser) -> None:
    # Arrange
    suitable_items_list = ['item_1', 'item_2']
    first_page_elements = ['element_1', 'element_2']
    first_page_items = ['item_1', 'item_2']
    selector = 'selector_dummy'

    parser.driver.find_elements_by_css_selector = mocker.MagicMock(return_value=first_page_elements)

    mocker.patch('shop_worker.Parser._get_elements_data',
                 return_value=first_page_items)
    mocker.patch('shop_worker.Parser._get_elements_number',
                 return_value=len(suitable_items_list))

    # Act
    items_list = parser._parse_elements(selector)

    # Assert
    assert items_list == suitable_items_list


def test_parse_elements_when_elements_number_on_page_less_then_total_elements_number(
        mocker: MockFixture, parser: Parser
) -> None:
    # Arrange
    suitable_items_list = ['item_1', 'item_2', 'item_3']
    selector = 'selector_dummy'

    first_page_elements = ['element_1']
    second_page_elements = ['element_2']
    third_page_elements = ['element_3']
    page_elements_dummy = [first_page_elements, second_page_elements, third_page_elements]

    first_page_items = ['item_1']
    second_page_items = ['item_2']
    third_page_items = ['item_3']
    items_dummy = [first_page_items, second_page_items, third_page_items]

    parser.driver.find_elements_by_css_selector = mocker.MagicMock(side_effect=page_elements_dummy)

    mocker.patch('shop_worker.Parser._get_elements_data',
                 side_effect=items_dummy)
    mocker.patch('shop_worker.Parser._get_elements_number',
                 return_value=len(suitable_items_list))
    mocker.patch('shop_worker.Parser._next_page')

    # Act
    items_list = parser._parse_elements(selector)

    # Assert
    assert items_list == suitable_items_list


def test_parse_elements_when_total_elements_number_not_multiple_of_page_elements_number(
        mocker: MockFixture, parser: Parser
) -> None:
    # Arrange
    suitable_items_list = ['item_1', 'item_2', 'item_3']
    selector = 'selector_dummy'

    first_page_elements = ['element_1', 'element_2']
    second_page_elements = ['element_3']
    page_elements_dummy = [first_page_elements, second_page_elements]

    first_page_items = ['item_1', 'item_2']
    second_page_items = ['item_3']
    items_dummy = [first_page_items, second_page_items]

    parser.driver.find_elements_by_css_selector = mocker.MagicMock(side_effect=page_elements_dummy)

    mocker.patch('shop_worker.Parser._get_elements_data',
                 side_effect=items_dummy)
    mocker.patch('shop_worker.Parser._get_elements_number',
                 return_value=len(suitable_items_list))
    mocker.patch('shop_worker.Parser._next_page')

    # Act
    items_list = parser._parse_elements(selector)

    # Assert
    assert items_list == suitable_items_list


def test_parse_elements_when_elements_list_is_empty(mocker: MockFixture, parser: Parser) -> None:
    # Arrange
    suitable_items_list = []
    page_elements_dummy = []
    selector = 'selector_dummy'
    parser.driver.find_elements_by_css_selector = mocker.MagicMock(return_value=page_elements_dummy)

    # Act
    items_list = parser._parse_elements(selector)

    # Assert
    assert items_list == suitable_items_list


def test_get_items_with_authorization(mocker: MockFixture, parser: Parser) -> None:
    # Arrange
    suitable_items_list = ['item_1', 'item_2', 'item_3']
    shop_link = 'shop_link_dummy'
    product_name = 'product_name_dummy'
    authorization_element_dummy = [1]

    mocker.patch('shop_worker.Parser._send_text')
    mocker.patch('shop_worker.Parser._push_button')
    parser.driver.find_elements_by_css_selector = mocker.MagicMock(side_effect=[authorization_element_dummy, []])
    mocker.patch('shop_worker.Parser._parse_elements', return_value=suitable_items_list)

    # Act
    items_list = parser.get_items(shop_link, product_name)

    # Assert
    assert items_list == suitable_items_list


def test_get_items_without_authorization(mocker: MockFixture, parser: Parser) -> None:
    # Arrange
    suitable_items_list = ['item_1', 'item_2', 'item_3']
    shop_link = 'shop_link_dummy'
    product_name = 'product_name_dummy'
    authorization_element_dummy = []
    mocker.patch('shop_worker.Parser._send_text')
    mocker.patch('shop_worker.Parser._push_button')
    parser.driver.find_elements_by_css_selector = mocker.MagicMock(return_value=authorization_element_dummy)
    mocker.patch('shop_worker.Parser._parse_elements', return_value=suitable_items_list)

    # Act
    items_list = parser.get_items(shop_link, product_name)

    # Assert
    assert items_list == suitable_items_list


def test_add_item_to_basket_when_item_added_to_basket(mocker: MockFixture, parser: Parser) -> None:
    # Arrange
    link = 'https://www.aliexpress.ru/item/1005003091886219.html'
    item = Item('name_dummy', 100, link)
    basket_wrapper_dummy = [1]
    mocker.patch('shop_worker.Parser._push_button')
    parser.driver.find_elements_by_css_selector = mocker.MagicMock(return_value=basket_wrapper_dummy)
    mocker.patch('shop_worker.Parser.check_basket', return_value=[item])

    # Act
    result = parser.add_item_to_basket(link)

    # Assert
    assert result is True


def test_add_item_to_basket_when_putting_item_to_basket_fails(mocker: MockFixture, parser: Parser) -> None:
    # Arrange
    link = 'https://www.aliexpress.ru/item/1005003091886219.html'
    item = Item('name_dummy', 100, link)
    basket_wrapper_dummy = []
    mocker.patch('shop_worker.Parser._push_button')
    parser.driver.find_elements_by_css_selector = mocker.MagicMock(return_value=basket_wrapper_dummy)
    mocker.patch('shop_worker.Parser.check_basket', return_value=[item])

    # Act
    result = parser.add_item_to_basket(link)

    # Assert
    assert result is False


def test_add_item_to_basket_when_item_link_and_items_in_basket_links_dont_match(mocker: MockFixture, parser: Parser) -> None:
    # Arrange
    link = 'https://www.aliexpress.ru/item/1005003091886219.html'
    item_1 = Item('name_dummy', 100, 'link_dummy_2')
    item_2 = Item('name_dummy', 100, 'link_dummy_2')
    basket_wrapper_dummy = [1]

    mocker.patch('shop_worker.Parser._push_button')
    parser.driver.find_elements_by_css_selector = mocker.MagicMock(return_value=basket_wrapper_dummy)
    mocker.patch('shop_worker.Parser.check_basket', return_value=[item_1, item_2])

    # Act
    result = parser.add_item_to_basket(link)

    # Assert
    assert result is False


def test_parse_basket_element(mocker: MockFixture, parser: Parser) -> None:
    # Arrange
    item = Item('title_dummy',
                Decimal('9011.43'),
                'https://www.aliexpress.ru/item/1005003091886219.html'
                )
    get_attributes_output = ['https://www.aliexpress.ru/item/1005003091886219.html?1568',
                             'title_dummy',
                             '9 011,43 руб.']
    mocker.patch("shop_worker.WebElement.get_attribute",
                 side_effect=get_attributes_output)
    mocker.patch("shop_worker.WebElement.find_element_by_css_selector",
                 return_value=WebElement)
    element = WebElement

    # Act
    item_test = parser._parse_basket_element(element)

    # Assert
    assert item_test.name == item.name
    assert item_test.link == item.link
    assert item_test.price == item.price


def test_check_basket_when_one_basket_link(mocker: MockFixture, parser: Parser) -> None:
    # Arrange
    suitable_items_list = ['item_1', 'item_2']
    basket_links_dummy = [WebElement]
    elements_dummy = ['element_1', 'element_2']
    parser.driver.find_elements_by_css_selector = mocker.MagicMock(side_effect=[basket_links_dummy, elements_dummy])

    mocker.patch('shop_worker.Parser._parse_basket_element',
                 side_effect=suitable_items_list)
    mocker.patch('shop_worker.WebElement.click')

    # Act
    items_list = parser.check_basket()

    # Assert
    assert items_list == suitable_items_list


def test_check_basket_when_two_basket_links(mocker: MockFixture, parser: Parser) -> None:
    # Arrange
    suitable_items_list = ['item_1', 'item_2']
    basket_links_dummy = [WebElement, WebElement]
    elements_dummy = ['element_1', 'element_2']

    parser.driver.find_elements_by_css_selector = mocker.MagicMock(side_effect=[basket_links_dummy, elements_dummy])

    mocker.patch('shop_worker.Parser._parse_basket_element',
                 side_effect=suitable_items_list)
    mocker.patch('shop_worker.WebElement.click')

    # Act
    items_list = parser.check_basket()

    # Assert
    assert items_list == suitable_items_list


def test_check_basket_when_basket_is_empty(mocker: MockFixture, parser: Parser) -> None:
    # Arrange
    suitable_items_list = []
    basket_links_dummy = [WebElement]
    elements_dummy = []

    parser.driver.find_elements_by_css_selector = mocker.MagicMock(side_effect=[basket_links_dummy, elements_dummy])

    mocker.patch('shop_worker.WebElement.click')

    # Act
    items_list = parser.check_basket()

    # Assert
    assert items_list == suitable_items_list
