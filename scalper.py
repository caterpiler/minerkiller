from decimal import Decimal
from entities import Item
from shop_worker import Parser
from logger import logger


class Scalper:
    def __init__(self, parser: Parser, shop_link: str) -> None:
        self.parser = parser
        self.shop_link = shop_link

    def scalp_item(self, item_name: str, max_buying_price: Decimal) -> Item:

        logger.info(f'Starting scalping an item {item_name} with price limit {max_buying_price}')
        found_items = self.parser.get_items(self.shop_link, item_name)

        suitable_items = [
            item
            for item in found_items
            if item.price <= max_buying_price
        ]

        suitable_items.sort(key=lambda x: x.price)
        
        for item in suitable_items:
            items_in_basket = self.parser.add_item_to_basket(item.link)
            if items_in_basket:
                logger.info(f'Sucessfully scalped the item {item.name} with price {item.price}')
                return item
        
        logger.info(f'Failed to sclap an item {item_name} with price limit {max_buying_price}')
        return None
