import os
import pathlib
import toml
import time
from decimal import Decimal

from utils import load_all_config_params
from shop_worker import Parser
from scalper import Scalper
from notifier import Notifier
from logger import logger


def main():
    # load config params
    with open(pathlib.Path(__file__).parent / 'config' / os.environ.get('ENV', 'local') / 'config.toml') as conf:
        conf = toml.load(conf)
        load_all_config_params(conf)

    shop_link = os.environ["aliexpress_shop_link"]
    pause_between_request = int(os.environ["pause_between_resquests_in_seconds"])

    notifyer = Notifier()
    parser = Parser()
    scalper = Scalper(parser, shop_link)

    print("MinerKiller scalper bot is running...")

    item_name = input("Enter item name: ")
    max_price = Decimal(input("Set max price limit [RUB]: "))

    print("Start scalping...")
    print("Press Ctrl+C to stop the programm")

    try:
        scalped_item = scalper.scalp_item(item_name, max_price)
        
        while scalped_item is None:
            time.sleep(pause_between_request)
            scalped_item = scalper.scalp_item(item_name, max_price)

        notifyer.send_message(f"The item {scalped_item.name}\nprice: {scalped_item.price} was added to basket!\nLink: {scalped_item.link}!")

    except KeyboardInterrupt:
        print("Programm has been stopped")
    
    except Exception:
        logger.exception("Exception occurred", exc_info=True)


if __name__ == "__main__":
    main()
