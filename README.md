## Poetry reference
1. Follow the instruction to [install poetry](https://python-poetry.org/docs/master/) ([guid on russian](https://pythonchik.ru/okruzhenie-i-pakety/menedzher-zavisimostey-poetry-polnyy-obzor-ot-ustanovki-do-nastroyki))
2. create poetry virtualenv and install all dependencies from poetry.lock: "poetry install"
3. Run virtual environment: "poetry shell"
4. add new module with "poetry add <lib_name>"

*Also you can install plagin for poetry in your IDE 