import toml
import pathlib
from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException
import chromedriver_autoinstaller
from typing import Dict

from slider_unlocker import SliderUnlocker
from captcha_solver import CaptchaSolver
from logger import logger
from exceptions import AuthenticationError


class Authenticator:
    def __init__(self, driver: WebDriver, selectors: Dict) -> None:
        self.driver = driver
        self.slider_unlocker = SliderUnlocker(driver)
        self.captcha_solver = CaptchaSolver(driver, selectors)
        self.selectors = selectors

    def _set_text(self, textbox_selector: str, text: str) -> None:
        element = self.driver.find_element_by_css_selector(textbox_selector)
        element.clear()
        element.send_keys(text)

    def _push_button(self, button_selector: str) -> None:
        button = self.driver.find_element_by_css_selector(button_selector)
        button.click()

    def _enter_username(self, username: str) -> None:
        self._set_text(self.selectors['LOGIN_TEXTBOX'], username)

    def _enter_password(self, password: str) -> None:
        self._set_text(self.selectors['PASSWORD_TEXTBOX'], password)

    def _unlock_slider(self) -> None:
        slider_button = self.driver.find_element_by_css_selector(self.selectors['SLIDER_BUTTON'])
        slider = self.driver.find_element_by_css_selector(self.selectors['SLIDER_FIELD'])
        self.slider_unlocker.unlock(slider_button, slider)

    def authenticate(self, login: str, password: str) -> None:
        try:
            self._enter_username(login)
            self._enter_password(password)
            self._push_button(self.selectors['AUTHORIZATION_BUTTON'])

            slider = self.driver.find_elements_by_css_selector(self.selectors['SLIDER_FIELD'])
            if len(slider) > 0:
                self._unlock_slider()
                captcha = self.driver.find_elements_by_css_selector(self.selectors['GET_NEW_CAPTCHA'])
                if len(captcha) > 0:
                    self.captcha_solver.solve_captcha()

                self._push_button(self.selectors['AUTHORIZATION_BUTTON'])

            authentication_error = self.driver.find_elements_by_css_selector(self.selectors['AUTHENTICATION_ERROR'])
            if len(authentication_error) > 0:
                logger.error("Authentication is failed, something went wrong")
                raise AuthenticationError

            logger.debug('Authentication completed succesfully')

        except NoSuchElementException as ex:
            logger.error(f"Authentication is failed due to missing page element: {ex.msg}")
            raise AuthenticationError


if __name__ == '__main__':

    with open(pathlib.Path(__file__).parent / 'ali_links.toml') as f:
        selectors = toml.load(f)

    shop_link = 'https://citilink.aliexpress.ru/'
    fake_login = 'fjodps'
    fake_password = 'fmps'

    chromedriver_autoinstaller.install()
    options = webdriver.ChromeOptions()
    options.add_experimental_option('excludeSwitches', ['enable-logging'])
    driver = webdriver.Chrome(options=options)
    driver.implicitly_wait(3)
    driver.get(shop_link)

    authenticator = Authenticator(driver, selectors)

    authorization_test_button = driver.find_element_by_css_selector(selectors['AUTHENTICATION_TEST'])
    authorization_test_button.click()

    for i in range(20):
        try:
            authenticator.authenticate(fake_login, fake_password)
        except AuthenticationError:

            continue

    driver.quit()