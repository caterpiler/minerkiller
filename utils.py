import os


def load_all_config_params(conf: dict) -> None:
    for key, value in conf.items():
        if isinstance(value, dict):
            load_all_config_params(value)
        else:
            os.environ[key] = str(value)
