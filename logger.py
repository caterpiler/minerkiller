import logging
import sys


logger = logging.getLogger('')
logger.setLevel(logging.DEBUG)

consoleLogsFormatter = logging.Formatter('%(levelname)s-%(message)s')
fileLogsFormatter = logging.Formatter('%(asctime)s-%(levelname)s-%(message)s')

log_to_file_handler = logging.FileHandler("app.log")
log_to_file_handler.setLevel(logging.ERROR)
log_to_file_handler.setFormatter(fileLogsFormatter)

log_to_console_handler = logging.StreamHandler(sys.stdout)
log_to_console_handler.setLevel(logging.DEBUG)
log_to_console_handler.setFormatter(consoleLogsFormatter)

logger.addHandler(log_to_file_handler)
logger.addHandler(log_to_console_handler)
