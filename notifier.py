import requests
import os
import toml
import pathlib

from utils import load_all_config_params


class Notifier:
    def __init__(self):
        self.tg_secret_key = os.environ["tg_secret_key"]
        self.tg_url = os.environ["tg_url"]
        self.tg_chat_id = os.environ["tg_chat_id"]

    def send_message(self, message):
        url = f'{self.tg_url}{self.tg_secret_key}/sendMessage'
        params = {
            "chat_id": self.tg_chat_id,
            "text": message
        }
        response = requests.get(url, params)
        return response


if __name__ == "__main__":
    with open(pathlib.Path(__file__).parent / 'config' / 'local' / 'config.toml') as conf:
        conf = toml.load(conf)
        load_all_config_params(conf)

    n = Notifier()
    r = n.send_message('test')
    print(r.json())