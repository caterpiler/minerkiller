import time
import chromedriver_autoinstaller
from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.remote.webelement import WebElement


class SliderUnlocker():
    def __init__(self, driver: WebDriver) -> None:
        self.driver = driver

    def unlock(self, button: WebElement, slider: WebElement) -> None:
        action = ActionChains(self.driver)
        action.move_to_element(button)
        action.click_and_hold(button)
        slider_width = slider.size['width']
        current_x_offset = 0
        x_offset = slider_width / 10
        while current_x_offset < slider_width:
            current_x_offset += x_offset
            action.move_by_offset(x_offset, 0)
        action.release()
        action.perform()


if __name__ == '__main__':
    chromedriver_autoinstaller.install()
    options = webdriver.ChromeOptions()
    options.add_experimental_option('excludeSwitches', ['enable-logging'])
    driver = webdriver.Chrome(options=options)
    driver.implicitly_wait(15)
    driver.get('http://demo.automationtesting.in/Slider.html')

    slider_button = driver.find_element_by_css_selector('.ui-slider-handle.ui-state-default.ui-corner-all')
    slider = driver.find_element_by_css_selector('.ui-slider.ui-slider-horizontal.ui-widget.ui-widget-content.ui-corner-all')
    
    unlocker = SliderUnlocker(driver)
    unlocker.unlock(slider_button, slider)
    
    time.sleep(1)
    driver.quit()

    print("slider_check_finished")
