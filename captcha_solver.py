import os
import re
import string
import numpy as np
import cv2
import pathlib
import base64
import onnxruntime
from selenium.webdriver.chrome.webdriver import WebDriver
from typing import Dict, Tuple

from logger import logger
from exceptions import CaptchaError

blank_index = 36
img_height = 30
img_width = 100
model_alphabet = string.ascii_uppercase + '0123456789'
model_dictionary = {number: letter for number, letter in enumerate(model_alphabet)}
attempts_number = 3
captcha_pattern = '[A-Z0-9]'


class CaptchaSolver:
    def __init__(self, driver: WebDriver, selectors: Dict) -> None:
        self.driver = driver
        model_path = os.path.join(pathlib.Path(__file__).parent, 'models', 'captcha_solver', 'captcha_model.onnx')
        self.model = onnxruntime.InferenceSession(model_path)
        self.selectors = selectors

    def _send_text(self, textbox_selector: str, text: str) -> None:
        element = self.driver.find_element_by_css_selector(textbox_selector)
        element.clear()
        element.send_keys(text)

    def _push_button(self, button_selector: str) -> None:
        button = self.driver.find_element_by_css_selector(button_selector)
        button.click()

    def _read_captcha(self, captcha_selector: str) -> Tuple[np.ndarray, str]:
        img_element = self.driver.find_element_by_css_selector(captcha_selector)
        img_data = img_element.find_element_by_css_selector('img').get_attribute('src')
        # Source data example - 'data:image/jpg;base64,/9j/4AAQSk...'
        # After 'base64,' is going base64 info
        img_base64 = img_data[img_data.find(',') + 1:]
        img_bytes = base64.b64decode(img_base64)
        img_arr = np.frombuffer(img_bytes, dtype=np.uint8)
        img = cv2.imdecode(img_arr, flags=cv2.IMREAD_COLOR)
        gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        gray_img = cv2.bitwise_not(gray_img)

        return gray_img, img_base64

    @staticmethod
    def _decode(predicted_indices: np.ndarray) -> str:
        predicted_label = ''
        for ind in predicted_indices:
            if ind != blank_index:
                predicted_label += model_dictionary[ind]
            else:
                predicted_label += '-'
        return predicted_label

    @staticmethod
    def _reshape_img(img: np.ndarray) -> np.ndarray:
        height, width = img.shape[:2]
        if height != img_height or width != img_width:
            img = cv2.resize(img, (img_width, img_height))
        return img

    def _process_captcha_onnx(self, img: np.ndarray) -> str:
        img_to_onnx = img.reshape(1, 1, img_height, img_width).astype(np.float32) / 255
        inputs = {self.model.get_inputs()[0].name: img_to_onnx}
        outs = self.model.run(None, inputs)
        predicted_np = outs[0]
        predicted_indices = np.argmax(predicted_np.squeeze(), axis=1)
        predicted_label = self._decode(predicted_indices)

        filtered_text = re.findall(captcha_pattern, predicted_label)
        if len(filtered_text) == 0:
            captcha_text = 'NOT R'
        else:
            captcha_text = ''.join(filtered_text)

        return captcha_text

    def solve_captcha(self) -> None:
        for attempt in range(attempts_number):
            logger.debug(f'Running CaptchaSolver.solve_captcha attempt {attempt}')
            img, img_base64 = self._read_captcha(self.selectors['CAPTCHA_IMAGE'])
            img = self._reshape_img(img)
            text = self._process_captcha_onnx(img)

            self._send_text(self.selectors['CAPTCHA_TEXTBOX'], text)
            self._push_button(self.selectors['CAPTCHA_BUTTON'])

            captcha_verified = self.driver.find_elements_by_css_selector(self.selectors['CAPTCHA_VERIFIED'])
            if len(captcha_verified) > 0:
                logger.debug('Captcha solved correctly')
                break
            else:
                logger.error(f'Captcha solver failed at attempt {attempt}')
                logger.debug(f'Image data: {img_base64}')
                logger.debug(f'Predicted captcha text: {text}')
                continue
        else:
            logger.error('Captcha solver failed')
            raise CaptchaError
